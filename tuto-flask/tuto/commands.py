from .app import manager, db
"""
@manager.command
def syncdb():
	db.create_all()
"""

@manager.command
def loaddb(filename):
	db.create_all()

	import yaml
	albums = yaml.load(open(filename))

	from .models import Albums

	for b in albums:
		o = Albums(id = b["entryId"],
			by= b["by"],
			img=b["img"],
			title =b["title"])
		db.session.add(o)
	db.session.commit()
